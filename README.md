# cli-library-status
Small command line utility for fetching how vacant UM learning spaces are.

## Usage
Get into your favourite Python 3 virtual environment and install the requirements.
```
pip install -r requirements.txt
```
Now simply run `library-status.py`. 
