#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
from datetime import time

try:
    site = requests.get("https://library.maastrichtuniversity.nl/services/hours-locations/")
except requests.exceptions.ConnectionError:
    print("Error: Unable to connect to library endpoint. Please check if your "
          "internet connection is working.")
    exit(1)
soup = BeautifulSoup(site.content, 'html5lib')

def parse_open_times(string):
    """Returns two time objects that contain information
    when the learning spaces open and when they close
    based on a string from the Library Hours & Locations
    website.
    """
    clean_str = string.replace('24:00', '23:59')
    open_str = clean_str[:5]
    closed_str = clean_str[-6:]
    open_ = time(int(open_str[:2]), int(open_str[3:]))
    closed = time(int(closed_str[:2]), int(closed_str[3:]))
    return open_, closed

class Location:
    def __init__(self, h3_tag):
        self.name = h3_tag.text # This is the header tag
        fullness_tag = h3_tag.next_sibling.next_sibling # The second next sibling is the tag containing fullness info
        circle_tag = fullness_tag.find('div', class_='circle') # The circle tag will contain the fullness %
        if circle_tag is not None: # if it exists, of course.
            self.full = float(circle_tag.get('data-percent'))
            self.open = True
        else: # If it doesn't, then there should be a closed tag
            closed_tag = fullness_tag.find('div', class_='closed')
            if closed_tag is not None: 
                self.full = 0
                self.open = False
            else: # Now if this boi isn't there the header tag we got is shit, abort everything
                raise ValueError("h3 tag does not contain learning location information")
            
        open_times_tag = fullness_tag.next_sibling
        self.open_times = parse_open_times(open_times_tag.text)
        
    def __str__(self):
        if self.open:
            return "{}: {:.0f}%".format(self.name, self.full)
        else:
            return "{}: Closed".format(self.name)

location_suspects = soup.find_all('h3')
locations = []
for suspect in location_suspects:
    try:
        locations.append(Location(suspect))
    except:
        pass

for location in locations:
    print(location)
